# mpv-autoload-levenshtein

This is a fork of the official [autoload](https://github.com/mpv-player/mpv/blob/master/TOOLS/lua/autoload.lua) mpv script which tries to be a bit smarter by trying to only autoload similar filenames, using a levenshtein calculation to determine filename similarity. **Note**: This is written to work with versions of `mpv` from 0.33 on.

While this does try to be a bit smarter, it won't work if the filenames are too short or just outright too dissimilar. This is intended to work when you have something like a sequence of similarly named videos, e.g. a series or a season of something. This means that multiple files like `01 - My Title`, `02 - My Other Title` are not likely to work properly as they will likely be too different to compare, especially if the dash truncation is in effect.

This fork also happens to try and autoload all files in a folder if the folder name has certain keywords, e.g. `Series` or `Season`, assuming that all files in the  folder should be automatically loaded and overriding the levenshtein calculation.

**Important Note**: There is not a guarantee that this modified script will work in all instances. From personal experience, I've had instances where I simply renamed files as it seems to become a bit tricky to try and accommodate all corner-cases.

## Usage

Copy the `autoload` folder into the `mpv` scripts directory, since, as noted above, this needs `mpv` from 0.33 on to work propertly. Configure as desired.

## Configuration

This does have some additional configuration (check the script for how to configure the `script-opts` file:)

| Option         | Comment                                                                                                                                                                                                                                                |
|:-------------- |:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| levenshtein    | Whether to use the levenshtein calculation.                                                                                                                                                                                                            |
| levdash        | Whether to truncate at the last dash (`-`) in a filename (can help for files with long episode titles).                                                                                                                                                |
| keywords       | Keywords to look for in folder names, which defaults to 'Series\|Season' and should be a pipe\-seperated (`\|`) list of keywords (avoid spaces). **Note**: In the autoload.conf, this does not need quotes around it but must still be pipe-separated. |
| keywords_case  | Whether the keyword check should be case sensitive.                                                                                                                                                                                                    |
| keywords_check | Whether to use the keyword check at all.                                                                                                                                                                                                               |

## Credits

Thanks go out to the following:

- The developer(s) who wrote and contributed to the original autoload scripts.

- [Badgerati](https://github.com/Badgerati) for their [levenshtein_algorithm.lua gist](https://gist.github.com/Badgerati/3261142), which this script uses along with a percentage similarity calculation.
