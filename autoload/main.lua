-- This script automatically loads playlist entries before and after the
-- the currently played file. It does so by scanning the directory a file is
-- located in when starting playback. It sorts the directory entries
-- alphabetically, and adds entries before and after the current file to
-- the internal playlist. (It stops if it would add an already existing
-- playlist entry at the same position - this makes it "stable".)
-- Add at most 5000 * 2 files when starting a file (before + after).

--[[
To configure this script use file autoload.conf in directory script-opts (the "script-opts"
directory must be in the mpv configuration directory, typically ~/.config/mpv/).

Example configuration would be:

disabled=no
images=no
videos=yes
audio=yes
additional_image_exts=list,of,ext
additional_video_exts=list,of,ext
additional_audio_exts=list,of,ext
ignore_hidden=yes
same_type=yes
levenshtein=yes
levdash=yes
keywords=Series|Season
keywords_case=false
keywords_check=true

--]]

MAXENTRIES = 5000

local msg = require 'mp.msg'
local options = require 'mp.options'
local utils = require 'mp.utils'
local lev = require 'levenshtein'

o = {
    disabled = false,
    images = true,
    videos = true,
    audio = true,
    additional_image_exts = "",
    additional_video_exts = "",
    additional_audio_exts = "",
    ignore_hidden = true,
    same_type = false,
    levenshtein = true,
    levdash = true,
    keywords = 'Series|Season',
    keywords_case = false,
    keywords_check = true
}
options.read_options(o, nil, function(list)
    split_option_exts(list.additional_video_exts, list.additional_audio_exts, list.additional_image_exts)
    if list.videos or list.additional_video_exts or
        list.audio or list.additional_audio_exts or
        list.images or list.additional_image_exts then
        create_extensions()
    end
end)

function Set (t)
    local set = {}
    for _, v in pairs(t) do set[v] = true end
    return set
end

function SetUnion (a,b)
    for k in pairs(b) do a[k] = true end
    return a
end

function Split (s)
    local set = {}
    for v in string.gmatch(s, '([^,]+)') do set[v] = true end
    return set
end

function customSplit(inputStr, sep)
    local errValue = false
    local strTable = {}
    -- If there's no separator, error out
    if sep == nil then
        errValue = true
        table.insert(strTable, inputStr)
        return strTable, errValue
    end

    for strVal in string.gmatch(inputStr, "([^"..sep.."]+)") do
        table.insert(strTable, strVal)
    end

    return strTable, errValue
end

EXTENSIONS_VIDEO = Set {
    '3g2', '3gp', 'avi', 'flv', 'm2ts', 'm4v', 'mj2', 'mkv', 'mov',
    'mp4', 'mpeg', 'mpg', 'ogv', 'rmvb', 'webm', 'wmv', 'y4m'
}

EXTENSIONS_AUDIO = Set {
    'aiff', 'ape', 'au', 'flac', 'm4a', 'mka', 'mp3', 'oga', 'ogg',
    'ogm', 'opus', 'wav', 'wma'
}

EXTENSIONS_IMAGES = Set {
    'avif', 'bmp', 'gif', 'j2k', 'jp2', 'jpeg', 'jpg', 'jxl', 'png',
    'svg', 'tga', 'tif', 'tiff', 'webp'
}

function split_option_exts(video, audio, image)
    if video then o.additional_video_exts = Split(o.additional_video_exts) end
    if audio then o.additional_audio_exts = Split(o.additional_audio_exts) end
    if image then o.additional_image_exts = Split(o.additional_image_exts) end
end
split_option_exts(true, true, true)

function create_extensions()
    EXTENSIONS = {}
    if o.videos then SetUnion(SetUnion(EXTENSIONS, EXTENSIONS_VIDEO), o.additional_video_exts) end
    if o.audio then SetUnion(SetUnion(EXTENSIONS, EXTENSIONS_AUDIO), o.additional_audio_exts) end
    if o.images then SetUnion(SetUnion(EXTENSIONS, EXTENSIONS_IMAGES), o.additional_image_exts) end
end
create_extensions()

function add_files(files)
    local oldcount = mp.get_property_number("playlist-count", 1)
    for i = 1, #files do
        mp.commandv("loadfile", files[i][1], "append")
        mp.commandv("playlist-move", oldcount + i - 1, files[i][2])
    end
end

function get_extension(path)
    match = string.match(path, "%.([^%.]+)$" )
    if match == nil then
        return "nomatch"
    else
        return match
    end
end

table.filter = function(t, iter)
    for i = #t, 1, -1 do
        if not iter(t[i]) then
            table.remove(t, i)
        end
    end
end

-- alphanum sorting for humans in Lua
-- http://notebook.kulchenko.com/algorithms/alphanumeric-natural-sorting-for-humans-in-lua

function alphanumsort(filenames)
    local function padnum(n, d)
        return #d > 0 and ("%03d%s%.12f"):format(#n, n, tonumber(d) / (10 ^ #d))
            or ("%03d%s"):format(#n, n)
    end

    local tuples = {}
    for i, f in ipairs(filenames) do
        tuples[i] = {f:lower():gsub("0*(%d+)%.?(%d*)", padnum), f}
    end
    table.sort(tuples, function(a, b)
        return a[1] == b[1] and #b[2] < #a[2] or a[1] < b[1]
    end)
    for i, tuple in ipairs(tuples) do filenames[i] = tuple[2] end
    return filenames
end

local autoloaded = nil

function get_playlist_filenames(playlist)
    local filenames = {}
    for i = 1, #playlist do
        local _, file = utils.split_path(playlist[i].filename)
        filenames[file] = true
    end
    return filenames
end

function find_and_add_entries()
    local path = mp.get_property("path", "")
    local dir, filename = utils.split_path(path)
    local dirFolders, dirSplit = customSplit(dir, "/")
    msg.debug(("dir: %s, filename: %s"):format(dir, filename))
    if o.disabled then
        msg.verbose("stopping: autoload disabled")
        return
    elseif #dir == 0 then
        msg.verbose("stopping: not a local path")
        return
    end

    -- Don't add entries if the playlist has been autoloaded already
    if (autoloaded == true) then
        return
    end

    local pl_count = mp.get_property_number("playlist-count", 1)
    this_ext = get_extension(filename)
    -- check if this is a manually made playlist
    if (pl_count > 1 and autoloaded == nil) or
       (pl_count == 1 and EXTENSIONS[string.lower(this_ext)] == nil) then
        msg.verbose("stopping: manually made playlist")
        return
    else
        autoloaded = true
    end

    if o.same_type then
        if EXTENSIONS_VIDEO[string.lower(this_ext)] ~= nil then
            EXTENSIONS_TARGET = EXTENSIONS_VIDEO
        elseif EXTENSIONS_AUDIO[string.lower(this_ext)] ~= nil then
            EXTENSIONS_TARGET = EXTENSIONS_AUDIO
        else
            EXTENSIONS_TARGET = EXTENSIONS_IMAGES
        end
    else
        EXTENSIONS_TARGET = EXTENSIONS
    end

    local pl = mp.get_property_native("playlist", {})
    local pl_current = mp.get_property_number("playlist-pos-1", 1)
    msg.debug(("playlist-pos-1: %s, playlist: %s"):format(pl_current,
        utils.to_string(pl)))

    local files = utils.readdir(dir, "files")
    if files == nil then
        msg.verbose("no other files in directory")
        return
    end
    table.filter(files, function (v, k)
        -- The current file could be a hidden file, ignoring it doesn't load other
        -- files from the current directory.
        if (o.ignore_hidden and not (v == filename) and string.match(v, "^%.")) then
            return false
        end
        local ext = get_extension(v)
        if ext == nil then
            return false
        end
        return EXTENSIONS_TARGET[string.lower(ext)]
    end)
    alphanumsort(files)

    if dir == "." then
        dir = ""
    end


    -- Check if the the keywords are in the current files' folder name and ignore the levenshtein
    -- calculation if so.
    local checkFolder = o.keywords_case and dirFolders[#dirFolders] or string.lower(dirFolders[#dirFolders])
    local keywords = o.keywords_case and customSplit(o.keywords, "|") or customSplit(string.lower(o.keywords), "|")
    if keywords_check ~= false and (#keywords > 0) then
        for key, keyword in pairs(keywords) do
            if string.find(checkFolder, keyword) ~= nil then
                o.levenshtein = false
                break
            end
        end
    end

    if (o.levenshtein) then
        -- We'll also check for the filename before the last dash on the assumption that a
        -- file that's part of a series may be named 01 - Title, 02 - Title, etc.
        local file, fileErr = {}, false
        local curFile, curFileErr = {}, false
        -- Get the filename without the extension
        local filenameNoExt = string.gsub(filename, "%.[%a%d]+$", "")
        if (o.levdash) then
            file, fileErr = customSplit(filenameNoExt, "-")
            if not (fileErr) then filenameNoExt = table.concat(file, "-",
                1, (#file > 1 and #file - 1 or 1)) end
        end
        local filesToRemove = {}
        for i = 1, #files do
            -- Remove the extension from the current file in the list
            local curFileNoExt = string.gsub(files[i], "%.[%a%d]+$", "")
            if (o.levdash) then
                curFile, curFileErr = customSplit(curFileNoExt, "-")
                if not (curFileErr) then curFileNoExt = table.concat(curFile, "-",
                    1, (#curFile > 1 and #curFile - 1 or 1)) end
            end
            -- Do a Levenshtein percentage match on the lowercase filenames. If it's lower than
            -- 65%, add the table position index to the start the filesToRemove list so that
            -- the items can be removed from last to first later.
            if levPercent(string.lower(filenameNoExt), string.lower(curFileNoExt)) < 65.0 then
                table.insert(filesToRemove, 1, i)
            end
            curFile = {}
            curFileErr = false
        end

        -- Go through the index values stored against the filesToRemove list and remove the
        -- from the last position to first, so we don't need to account for the changing
        -- position of items in the files list.
        for i = 1, #filesToRemove do
            table.remove(files, filesToRemove[i])
        end
    end

    -- Find the current pl entry (dir+"/"+filename) in the sorted dir list
    local current
    for i = 1, #files do
        if files[i] == filename then
            current = i
            break
        end
    end
    if current == nil then
        return
    end
    msg.debug("current file position in files: "..current)

    local append = {[-1] = {}, [1] = {}}
    local filenames = get_playlist_filenames(pl)
    for direction = -1, 1, 2 do -- 2 iterations, with direction = -1 and +1
        for i = 1, MAXENTRIES do
            local pos = current + i * direction
            local file = files[pos]
            if file == nil or file[1] == "." then
                break
            end

            local filepath = dir .. file
            -- skip files already in playlist
            if not filenames[file] then
                if direction == -1 then
                    msg.info("Prepending " .. file)
                    table.insert(append[-1], 1, {filepath, pos - 1})
                else
                    msg.info("Adding " .. file)
                    if pl_count > 1 then
                        table.insert(append[1], {filepath, pos - 1})
                    else
                        mp.commandv("loadfile", filepath, "append")
                    end
                end
            end
        end
        if pl_count == 1 and direction == -1 and #append[-1] > 0 then
            for i = 1, #append[-1] do
                mp.commandv("loadfile", append[-1][i][1], "append")
            end
            mp.commandv("playlist-move", 0, current)
        end
    end

    if pl_count > 1 then
        add_files(append[1])
        add_files(append[-1])
    end
end

mp.register_event("start-file", find_and_add_entries)
